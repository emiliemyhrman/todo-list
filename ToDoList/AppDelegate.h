//
//  AppDelegate.h
//  ToDoList
//
//  Created by Emilie Myhrman on 2016-02-14.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

