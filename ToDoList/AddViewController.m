//
//  AddViewController.m
//  ToDoList
//
//  Created by Emilie Myhrman on 2016-02-14.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()
@property () UIColor *selectedColor;
@property (weak, nonatomic) IBOutlet UITextField *taskName;
@property (weak, nonatomic) IBOutlet UISwitch *importantSwitch;

@end

@implementation AddViewController

- (IBAction)importantSwitch:(id)sender {
    self.importantSwitch.enabled = NO;
}

- (IBAction)addTask:(id)sender {
    
    if (self.importantSwitch.enabled == YES) {
        // Får inte riktigt detta att fungera, vill ändra textfärg när man väljer important. Men det sparas inte över till arrayen.
        [self.tasks addObject:self.taskName.text];
        [self.taskName setTextColor:[UIColor blueColor]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (self.importantSwitch.enabled == NO){
        [self.tasks addObject:self.taskName.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
